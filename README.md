# clevo-audio-mode
Program to provide colors from audio to Clevo keyboard backlights. Uses the kernel module provided by [clevo-xsm-wmi](https://bitbucket.org/tuxedocomputers/clevo-xsm-wmi/overview). This program works by modifying the custom colors provided by sysfs.

## Installation
You must build the kernel module from source and patch it. The patch is provided in this repo under `clevo-xsm-wmi.c.patch`. The patch allows the module to read hexadecimal values from sysfs so that it can set more than 8 different custom colors.

Then just run `make` in this repo to build the program itself (and `sudo make install` if you want to make it accessible everywhere).

Currently, the only source available is from `pulseaudio`, but you are free to provide others.

## Usage
This program works best with the brightness level turned all the way up. Just run `sudo clevo-audio-mode` and it will output data from your system's audio to the Clevo keyboard color sysfs setting.

## References
[cli-visualizer](https://github.com/dpayne/cli-visualizer)

## [Demo](https://www.youtube.com/playlist?list=PLnNQzdl05rzBVPLHeCca478TPtgrGdNY2)

[![demo](https://img.youtube.com/vi/xz8XKwldlNg/0.jpg)](https://www.youtube.com/playlist?list=PLnNQzdl05rzBVPLHeCca478TPtgrGdNY2)

[(Hosted on Youtube)](https://www.youtube.com/playlist?list=PLnNQzdl05rzBVPLHeCca478TPtgrGdNY2)


## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
