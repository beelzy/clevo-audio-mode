#include <math.h>

static int* hsv_to_rgb(float h, float s, float v) {
        int *rgb = new int[3];

        int i;
        float f;
        float p;
        float q;
        float t;
        float r;
        float g;
        float b;

        if (s == 0) {
                r = v;
                g = v;
                b = v;
        } else {
                h *= 6;
                i = floor(h);
                f = h - i;
                p = v * (1 - s);
                q = v * (1 - s * f);
                t = v * (1 - s * (1 - f));

                switch(i) {
                        case 0:
                                r = v;
                                g = t;
                                b = p;
                                break;
                        case 1:
                                r = q;
                                g = v;
                                b = p;
                                break;
                        case 2:
                                r = p;
                                g = v;
                                b = t;
                                break;
                        case 3:
                                r = p;
                                g = q;
                                b = v;
                                break;
                        case 4:
                                r = t;
                                g = p;
                                b = v;
                                break;
                        default:
                                r = v;
                                g = p;
                                b = q;
                                break;
                }
        }
        rgb[0] = r * 255;
        rgb[1] = g * 255;
        rgb[2] = b * 255;

        return rgb;
}
