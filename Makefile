TARGET= clevo-audio-mode

GCC := $(shell which gcc)

DIR=$(shell pwd)
BUILD_DIR = $(DIR)/build
PREFIX = /bin/

CXX_FLAGS = -std=c++14
CXX_FLAGS += -D_ENABLE_PULSE
LD_FLAGS = $(LDFLAGS)
LD_FLAGS += -D_ENABLE_PULSE

LIBS = -lm -lstdc++
LIBS += -lpulse -lpulse-simple

SOURCES= $(wildcard *.cpp)

HEADERS= $(wildcard *.h)

OBJECTS= $(addprefix $(BUILD_DIR)/,$(notdir $(SOURCES:.cpp=.o)))

all: prepare build

.PHONY: prepare
prepare:
	mkdir -p $(BUILD_DIR)
	rm -f $(BUILD_DIR)/$(TARGET)

.PHONY: build
build: $(OBJECTS) $(TARGET)

.PHONY:clean
clean:
	@rm -rf $(BUILD_DIR)

uninstall:
	@rm -f $(PREFIX)/$(TARGET)

install:
	cp $(BUILD_DIR)/$(TARGET) $(PREFIX)

$(BUILD_DIR)/%.o: %.cpp
	$(CXX) $(CXX_FLAGS) $(LD_FLAGS) $(INCLUDE_PATH) -c $< -o $@

$(TARGET): $(OBJECTS)
	$(CXX) $(CXX_FLAGS) $(LDFLAGS) $(INCLUDE_PATH) $(LIB_PATH) -o $(BUILD_DIR)/$(TARGET) $(OBJECTS) $(LIBS)


