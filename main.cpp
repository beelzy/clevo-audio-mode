#include "pasource.h"
#include "color.h"

#include <algorithm>
#include <csignal>
#include <cstdio>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <thread>
#include <sstream>
#include <string>

using namespace std;

namespace
{
        volatile std::sig_atomic_t exitStatus;

        const float alpha = 1.0 - expf( (-2.0 * M_PI) / (0.0001 * PASource::k_sample_rate) );
        float FALLOFF_WEIGHT = 0.95;
        int SAMPLE_SIZE = 4400;
        float FREQUENCY_SCALING = 4; 
        // If sampled values are too similar, discard them to save on changes to sysfs.
        float DELTA = 50;

        // Ensure we write similar values over time, or else we may be stuck on the wrong value for too long.
        int INTERVAL = 100;
        clock_t deltaTime = 0;
        unsigned int frames = 0;

        float color_shift = 0.0f;
}

void shutdown_sig(int sig)
{
        cout << "\n";
        exitStatus = sig;
}

float clamp_color(int value, float scaling = FREQUENCY_SCALING)
{
        return std::min(std::max((const float) value / SAMPLE_SIZE / scaling, 0.0f), 1.0f);
}

// Obtain totals with envelope detector
void buffer_total(PASource::pcm_stereo_sample* total, PASource::pcm_stereo_sample* buffer)
{
        float total_l = 0.0f;
        float total_m = 0.0f;
        float total_r = 0.0f;

        int i;
        for (i = 0; i < SAMPLE_SIZE; i++) {
             total_l = total_l + alpha * (fabsf(buffer[i].l) - total_l);
             total_m = total_m + alpha * (fabsf(buffer[i].m) - total_m);
             total_r = total_r + alpha * (fabsf(buffer[i].r) - total_r);
        }
        total->l = int(total_l);
        total->m = int(total_m);
        total->r = int(total_r);
}

void apply_falloff(PASource::pcm_stereo_sample* buffer, PASource::pcm_stereo_sample* falloff)
{

        auto falloff_value = std::min(falloff->l * FALLOFF_WEIGHT, falloff->l - 1.0f);

        falloff->l = std::max(int(falloff_value), int(buffer->l));
        
        falloff_value = std::min(falloff->m * FALLOFF_WEIGHT, falloff->m - 1.0f);

        falloff->m = std::max(int(falloff_value), int(buffer->m));
        
        falloff_value = std::min(falloff->r * FALLOFF_WEIGHT, falloff->r - 1.0f);

        falloff->r = std::max(int(falloff_value), int(buffer->r));
}

int main(int argc, char*argv[]) {
        std::ios::sync_with_stdio(false);

        exitStatus = 0;
        std::signal(SIGINT, shutdown_sig);
        std::signal(SIGTERM, shutdown_sig);

        std::ostringstream ss;
        ss.precision(5);

        PASource::pcm_stereo_sample *m_pcm_buffer = static_cast<PASource::pcm_stereo_sample *>(calloc(44100, sizeof(PASource::pcm_stereo_sample)));
        PASource::pcm_stereo_sample *falloff = static_cast<PASource::pcm_stereo_sample *>(malloc(sizeof(PASource::pcm_stereo_sample)));
        PASource::pcm_stereo_sample *previous = static_cast<PASource::pcm_stereo_sample *>(malloc(sizeof(PASource::pcm_stereo_sample)));
        PASource::pcm_stereo_sample *average = static_cast<PASource::pcm_stereo_sample *>(malloc(sizeof(PASource::pcm_stereo_sample)));

        float diff = 0;

        PASource* pasource = new PASource(); 

        ofstream color_file;

        while (exitStatus == 0)
        {
                clock_t beginFrame = clock();

                if (pasource->read(m_pcm_buffer, SAMPLE_SIZE))
                {
                        buffer_total(average, m_pcm_buffer);
                        apply_falloff(average, falloff);
                        if (fabs(average->l - previous->l) > DELTA || fabs(average->m - previous->m) > DELTA || fabs(average->r - previous->r) > DELTA ||
                                        (deltaTime >= INTERVAL && 
                                         (average->l != previous->l || average->m != previous->m || average->r != previous->r))) {
                                float lb = clamp_color(average->l);
                                float mb = clamp_color(average->m);
                                float rb = clamp_color(average->r);

                                float ls = clamp_color(falloff->l, 2.0f);
                                float ms = clamp_color(falloff->m, 2.0f);
                                float rs = clamp_color(falloff->r, 2.0f);

                                float lh = color_shift; //fmod(clamp_color(falloff->m) + color_shift, 1.0f);
                                float mh = fmod(color_shift + 0.333, 1.0f); //fmod(clamp_color(falloff->r) + 0.333 + color_shift, 1.0f);
                                float rh = fmod(color_shift + 0.666, 1.0f); //fmod(clamp_color(falloff->l) + 0.666 + color_shift, 1.0f);

                                int* lrgb = hsv_to_rgb(lh, ls, lb);
                                int* mrgb = hsv_to_rgb(mh, ms, mb);
                                int* rrgb = hsv_to_rgb(rh, rs, rb);

                                diff = (fabs(previous->l - average->l) + fabs(previous->m - average->m) + fabs(previous->r - average->r)) / 3.0f;
                                previous->l = average->l;
                                previous->m = average->m;
                                previous->r = average->r;

                                color_file.open("/sys/devices/platform/clevo_xsm_wmi/kb_color");

                                ss.str(std::string());
                                ss << std::hex << std::setw(6) << std::setfill('0') << lrgb[0] + lrgb[1] * 256 + lrgb[2] * 256 * 256;
                                std::string l(ss.str());

                                ss.str(std::string());
                                ss << std::hex << std::setw(6) << std::setfill('0') << mrgb[0] + mrgb[1] * 256 + mrgb[2] * 256 * 256;
                                std::string m(ss.str());

                                ss.str(std::string());
                                ss << std::hex << std::setw(6) << std::setfill('0') << rrgb[0] + rrgb[1] * 256 + rrgb[2] * 256 * 256;
                                std::string r(ss.str());

                                cout << std::fixed << "\rL:" << l << " M:" << m << " R:" << r << std::flush;
                                color_file << l << " " << m << " " << r;
                                color_file.close();

                                free(lrgb);
                                free(mrgb);
                                free(rrgb);
                        }
                }
                else {
                        std::this_thread::sleep_for(std::chrono::milliseconds(100ul));
                }
                clock_t endFrame = clock();

                deltaTime += (endFrame - beginFrame) % INTERVAL;
                frames++;
                // shift colors
                float speed = clamp_color(diff);
                color_shift = fmod(color_shift + 0.2 * speed, 1.0f);
        }

        if (m_pcm_buffer != nullptr)
        {
                free(m_pcm_buffer);
        }
        if (falloff != nullptr)
        {
                free(falloff);
        }
        if (previous != nullptr)
        {
                free(previous);
        }
        if (average != nullptr)
        {
                free(average);
        }
        if (pasource != nullptr)
        {
                free(pasource);
        }
}
