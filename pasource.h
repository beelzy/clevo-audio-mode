#include <fstream>
#include <memory>

#include <pulse/error.h>
#include <pulse/pulseaudio.h>
#include <pulse/simple.h>

class PASource {
        public:
                PASource();

                ~PASource();

                struct pcm_stereo_sample
                {
                        int16_t l;
                        int16_t m;
                        int16_t r;
                };

                bool read(pcm_stereo_sample *buffer, uint32_t buffer_size);
                static const int32_t k_sample_rate = 44100;

        private:
                pa_simple *m_pulseaudio_simple;

                pa_mainloop *m_pulseaudio_mainloop;

                std::string m_pulseaudio_default_source_name;

                std::string audio_device;

                bool audio_device_initialized;

                static void pulseaudio_context_state_callback(pa_context *c, void *userdata);

                static void pulseaudio_server_info_callback(pa_context *context, const pa_server_info *i, void *userdata);

                void populate_default_source_name();

                bool open_pulseaudio_source(uint32_t max_buffer_size);
};
