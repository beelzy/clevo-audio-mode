#include "pasource.h"

#include <cerrno>
#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <sys/stat.h>
#include <sys/types.h>

#include <unistd.h>

namespace {
        static const char k_record_stream_name[] = "clevo";
        static const char k_record_stream_description[] = "clevo stream";

        static const int32_t k_channels = 3;

        static const std::string k_default_monitor_postfix = ".monitor";
       }

PASource::PASource() : m_pulseaudio_simple{nullptr}, m_pulseaudio_mainloop{nullptr}, audio_device_initialized(false) { 
}

void PASource::pulseaudio_server_info_callback(
                pa_context *, const pa_server_info *i, void *userdata)
{
        if (i != nullptr)
        {
                PASource *clevo_pa_src = reinterpret_cast<PASource *>(userdata);
                std::string name = i->default_sink_name;
                name.append(k_default_monitor_postfix);

                clevo_pa_src->m_pulseaudio_default_source_name = name;

                pa_mainloop_quit(clevo_pa_src->m_pulseaudio_mainloop, 0);
        }
}

void PASource::pulseaudio_context_state_callback(pa_context *c, void *userdata)
{
        switch (pa_context_get_state(c))
        {
        case PA_CONTEXT_UNCONNECTED:
        case PA_CONTEXT_CONNECTING:
        case PA_CONTEXT_AUTHORIZING:
        case PA_CONTEXT_SETTING_NAME:
                break;

        case PA_CONTEXT_READY:
                {
                        pa_operation_unref(pa_context_get_server_info(c, pulseaudio_server_info_callback, userdata));
                        break;
                }

        case PA_CONTEXT_FAILED:
        case PA_CONTEXT_TERMINATED:
                PASource *clevo_pa_src = reinterpret_cast<PASource *>(userdata);
                pa_mainloop_quit(clevo_pa_src->m_pulseaudio_mainloop, 0);
                break;
        }
}

void PASource::populate_default_source_name()
{
        pa_mainloop_api *mainloop_api;
        pa_context *pulseaudio_context;

        m_pulseaudio_mainloop = pa_mainloop_new();

        mainloop_api = pa_mainloop_get_api(m_pulseaudio_mainloop);
        pulseaudio_context = pa_context_new(mainloop_api, "clevo device list");

        pa_context_connect(pulseaudio_context, nullptr, PA_CONTEXT_NOFLAGS, nullptr);

        pa_context_set_state_callback(pulseaudio_context, pulseaudio_context_state_callback, reinterpret_cast<void *>(this));

        int ret;
        if (pa_mainloop_run(m_pulseaudio_mainloop, &ret) < 0)
        {
                printf("Could not open pulseaudio mainloop to find default device name: %d", ret);
        }
}

bool PASource::open_pulseaudio_source(const uint32_t max_buffer_size)
{
        int32_t error_code = 0;

        static const pa_sample_spec sample_spec = {PA_SAMPLE_S16LE, k_sample_rate, k_channels};

        static const pa_buffer_attr buffer_attr = {max_buffer_size, 0, 0, 0, (max_buffer_size / 3)};

        if (!audio_device_initialized)
        {
                populate_default_source_name();

                if (!m_pulseaudio_default_source_name.empty())
                {
                        m_pulseaudio_simple = 
                                pa_simple_new(nullptr, k_record_stream_name, PA_STREAM_RECORD,
                                                m_pulseaudio_default_source_name.c_str(),
                                                k_record_stream_description, &sample_spec,
                                                nullptr, &buffer_attr, &error_code);
                        audio_device = m_pulseaudio_default_source_name;
                }

                if (m_pulseaudio_simple == nullptr)
                {
                        m_pulseaudio_simple =
                                pa_simple_new(nullptr, k_record_stream_name, PA_STREAM_RECORD,
                                                nullptr, k_record_stream_description,
                                                &sample_spec, nullptr, &buffer_attr, &error_code);
                }

                if (m_pulseaudio_simple == nullptr)
                {
                        m_pulseaudio_simple =
                                pa_simple_new(nullptr, k_record_stream_name, PA_STREAM_RECORD,
                                                "0", k_record_stream_description, &sample_spec,
                                                nullptr, &buffer_attr, &error_code);
                        audio_device = "0";
                }
                audio_device_initialized = true;
        }
        else
        {
                m_pulseaudio_simple =
                        pa_simple_new(nullptr, k_record_stream_name, PA_STREAM_RECORD,
                                        audio_device.c_str(), k_record_stream_description,
                                        &sample_spec, nullptr, &buffer_attr, &error_code);
        }

        if (m_pulseaudio_simple != nullptr)
        {
                return true;
        }

        printf("Could not open pulseaudio source %s: %s", audio_device.c_str(), pa_strerror(error_code));

        return false;
}

bool PASource::read(pcm_stereo_sample *buffer, const uint32_t buffer_size)
{
        auto buffer_size_bytes = static_cast<size_t>(sizeof(pcm_stereo_sample) * buffer_size);

        if (m_pulseaudio_simple == nullptr)
        {
                open_pulseaudio_source(static_cast<uint32_t>(buffer_size_bytes));
        }

        if (m_pulseaudio_simple != nullptr)
        {
                memset(buffer, 0, buffer_size_bytes);

                int32_t error_code;

                auto return_code = pa_simple_read(m_pulseaudio_simple, buffer, buffer_size_bytes, &error_code);

                if (return_code < 0)
                {
                        printf("Could not finish reading pulse audio stream buffer, bytes read: %d buffer size: %d",
                                        return_code, buffer_size_bytes);

                        memset(buffer, 0, buffer_size_bytes);

                        pa_simple_free(m_pulseaudio_simple);
                        m_pulseaudio_simple = nullptr;

                        return false;
                }

                return true;
        }

        memset(buffer, 0, buffer_size_bytes);

        return false;
}

PASource::~PASource()
{
        if (m_pulseaudio_simple != nullptr)
        {
                pa_simple_free(m_pulseaudio_simple);
        }
}
